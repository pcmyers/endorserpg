class Signupper < ActionMailer::Base
  default from: "youdorsetest@pcmyers.com"
  
  def welcome(recipient)
    @applicant = recipient 
    mail(:to => recipient.emails)    

  end

end
