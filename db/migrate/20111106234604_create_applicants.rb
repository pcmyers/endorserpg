class CreateApplicants < ActiveRecord::Migration
  def change
    create_table :applicants do |t|
      t.string :emails
      t.string :link

      t.timestamps
    end
  end
end
