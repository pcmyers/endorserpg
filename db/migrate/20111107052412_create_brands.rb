class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :owner
      t.string :company
      t.string :link

      t.timestamps
    end
  end
end
